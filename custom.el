(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("16dd114a84d0aeccc5ad6fd64752a11ea2e841e3853234f19dc02a7b91f5d661" default))
 '(org-structure-template-alist
   '(("n" . "notes")
     ("a" . "export ascii")
     ("c" . "center")
     ("C" . "comment")
     ("e" . "example")
     ("E" . "export")
     ("h" . "export html")
     ("l" . "export latex")
     ("q" . "quote")
     ("s" . "src")
     ("v" . "verse")))
 '(package-selected-packages
   '(ess base16-theme flycheck ag color-theme color-theme-solarized projectile dockerfile-mode emamux markdown-mode jabber json erc-hl-nicks dired-subtree yasnippet yaml-mode emmet-mode expand-region smex flx-ido json-mode js2-mode znc jinja2-mode nginx-mode jedi-direx erlang go-mode multiple-cursors less-css-mode unicode-fonts web-mode smartparens scss-mode ox-reveal find-file-in-repository f ercn erc-view-log autopair)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "gray5" :foreground "wheat4" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :foundry "nil" :family "Monaco"))))
 '(mode-line ((t (:background "black" :foreground "cyan" :inverse-video t :box nil)))))
