===================================
~p/.emacs.d (my own Emacs config)
===================================

This is my Emacs configuration (including custom theme).

Some code is taken/inspired from various places around the internet, mainly the following:

- `EmacsWiki <http://www.emacswiki.org/>`_
- `Emacs Rocks! <http://emacsrocks.com/>`_
- `What the .emacs.d!? <http://whattheemacsd.com/>`_
- `Tealeg's Emacs configuration <https://github.com/tealeg/dot-emacs-dot-d>`_