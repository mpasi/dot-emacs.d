;;; init.el --- Emacs init file

;;; Commentary:
;;;   Emacs default config file.
;;;   This is the entry point to load other config files.

;;; Code:

(package-initialize)

(add-to-list 'load-path (concat user-emacs-directory "lisp"))
(add-to-list 'load-path (expand-file-name "~/share/emacs/site-lisp/"))
;(add-to-list 'load-path (expand-file-name "~/share/emacs/site-lisp/markdown"))
(add-to-list 'load-path "/opt/local/share/emacs/site-lisp")
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp")

(server-start)

(require 'p-init)
(require 'p-custom)
(require 'p-packages)
(require 'p-private)
(require 'p-backup)
(require 'p-interactive)                ;; interactive commands
(require 'p-utils)
(require 'p-editing)
(require 'p-modes)
(require 'p-bindings)
;(require 'ack-irc)
;(require 'ack-irc-commands)
;(require 'ack-jabber)
(require 'p-automode)
(require 'p-appearance)
(require 'p-python)

(require 'p-calc)
(require 'p-latex)
(require 'p-org)

(provide 'init)
;;; init.el ends here
