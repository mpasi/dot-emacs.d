;;; p-custom.el --- Configuration for Emacs custom

;;; Commentary:
;;;   Configuration for Emacs custom system.

;;; Code:

(setq custom-file (concat user-emacs-directory "custom.el"))
(load custom-file)

(provide 'p-custom)
;;; p-custom.el ends here
