;;; p-utils.el --- Utilities

;;; Commentary:
;;;   General utilities.

;;; Code:

;;;;;;;;;;;;;;;;;;;;;;;;;;;; HTMLIZE WoB print ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defadvice htmlize-buffer-1 (around black-on-white first activate)  
 "Advise a core function used by htmlize-buffer to use black on
white for the main text. This is useful if you mainly use bright
foreground colors on dark background.
(c) Bjoern Rueffer 2010"
 (let ((fg (face-attribute 'default :foreground))
       (bg (face-attribute 'default :background)))   
   (set-face-foreground 'default "black")   
   (set-face-background 'default "white")   
   ad-do-it
   (set-face-foreground 'default fg)   
   (set-face-background 'default bg)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;; GPG SETUP ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;"
;; add easypg at the end, since it's needed only by /usr/bin/emacs
;; (when (not (display-graphic-p))
;;   (add-to-list 'load-path (expand-file-name"~/share/emacs/site-lisp/epg"))
;;   (require 'epa-setup)
;;   (epa-file-enable))


(defun do-replace (reg rep start end)
  (goto-char start)
  (setq num 0)
  ;; (message "start %s %d" reg (point))
  (condition-case ()
      (while (re-search-forward reg end t)
	;; (message "at %s %d" reg (point))
	(replace-match rep nil nil)
	(setq num (+ num 1)))
    (error))
  (message "Replaced %d times" num))

(provide 'p-utils)
;;; p-utils.el ends here
