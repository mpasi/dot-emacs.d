;;; p-other.el --- Miscellaneous stuff

;;; Commentary:
;;;   Commentaries are overrated.

;;; Code:

(defun remove-pmids-cfr ()
  "Use when PMID files from two crawls share the same pmids.
   Get two buffers on near windows so that:
    - NEW (redundant) is previous
    - OLD (redundancy) is next
   then place the point at the beginning of a pmid line, and use
   this function to *** remove all pmids in OLD from NEW *** for
   the current line."
  (interactive)
  (condition-case err
      (while (< (point) (line-end-position))
	(let ((beg (point)))
	  (re-search-forward ",")
	  (setq pmid (buffer-substring beg (point)))
	  (kill-region beg (point))
	  (p-prev-window)
	  (setq old (point))
	  (re-search-forward pmid nil t)
	  (message "looking for :%s: found %d->%d" pmid old (point))
	  (if (not (= old (point)))
	      (progn
		(message "replacing! %d-%d" (match-beginning 0) (match-end 0))
		(replace-match "")))
	  (p-next-window)))
    (error
     (message "error? %s" err))))
	       
(global-set-key "\C-cx" 'remove-pmids-cfr)


;;;;;;;; INGRESS
(define-prefix-command 'ingress-functions-map)
(global-set-key "\C-ci" 'ingress-functions-map)
(define-key ingress-functions-map "r" 'strrev)

(defun reverse-words (beg end)
  "Reverse the order of words in region."
  (interactive "*r")
  (apply
   'insert
   (reverse
    (split-string
     (delete-and-extract-region beg end) "\\b"))))
(defun reverse-string (start end)
  "Reverse string"
  (interactive "r")
  (kill-region start end)
  (insert (apply #'string (reverse (string-to-list (car kill-ring-yank-pointer))))))

(defun ogame-mode ()
  (interactive)
  (local-set-key [mouse-2] 'ogame-mouse-yank-at-click)
  )

(defun ogame-mouse-yank-at-click (click arg)
  "yank and insert - line"
  (interactive "e\nP")
  (mouse-yank-at-click click arg)
  (insert "\n-------------------------------------------------------------------------------\n")
  )

(provide 'p-other)
;;; p-other.el ends here
