;;; p-init.el --- Initialization helpers

;;; Commentary:
;;;   Helper functions and variables for Emacs initialization.

;;; Code:

(setq exec-path (cons "~/bin/" exec-path)
      shell-file-name "/bin/zsh")

(defvar cache-dir (concat user-emacs-directory "cache"))

(defun file-path-in-cache-dir (filename)
  "Return the full path for FILENAME in the cache directory."
  (concat (file-name-as-directory cache-dir) filename))

(provide 'p-init)
;;; p-init.el ends here
