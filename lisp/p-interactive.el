;;; p-interactive.el --- Interactive commands

;;; Commentary:
;;;   Interactive commands.

;;; Code:

;; (require 'compile)



;;; Date & Time stuff
(defun insert-date-short ()
  "Insert very short date"
  (interactive)
  (insert (format-time-string "%y%m%d" (current-time))))

(defun insert-date (format)
  "Insert date at point according to format string"
  (interactive "*sInsert format string: ")
  (if (string-match "^$" format)
      (insert (format-time-string "%a %b %d %T %Z %Y" (current-time)))
    (insert (format-time-string format (current-time)))
    ))

(defun insert-time ()
  "Insert time at point"
  (interactive)
  (insert (format-time-string "%T" (current-time))))



;;; Justifying & Filling
(defun my-set-fill-column (point)
  "Fill current region and indent it"
  (interactive "d")
  (set-fill-column (current-column))
  )

(defun my-justify-region (start end)
  "Fill current region and justify it"
  (interactive "r")
  (fill-region start end 'full)
  )



;;; Counting
(defun wc-region (start end)
  "Print number of words in the region."
  (interactive "r")
  (save-excursion
    (let ((n 0))
      (goto-char start)
      (while (< (point) end)
        (if (forward-word 1)
            (setq n (1+ n))))
    (message "Region has %d words" n) n)
    ))

(defun count-region (start end)
   "Print number of lines words and characters in the region."
   (interactive "r")
   (message "Region has %d lines, %d words, %d characters"
 	   (count-lines start end) 
           (wc-region start end) 
           (- end start)))



;;; License
(defun licenseify (type)
  (interactive "sTYPE([p]ublic_domain,[g]pl,[G]pl-academic):")
  (cond ((string-match "p" type)
	 (setq copying
	       '(
		 "This program is in the public domain, and can be used, modified"
		 "and/or redistributed, as long as this notice is available with it."
		 "\"It's everybody's, it's the discoteque's\""
		 )))
	((string-match "g" type)
	 (setq copying
	       '(
		 "This program is free software; you can redistribute it and/or modify"
		 "it under the terms of the GNU General Public License as published by"
		 "the Free Software Foundation; either version 2 of the License, or"
		 "(at your option) any later version."
		 )))
	)
  
  (save-excursion
    (beginning-of-buffer)
    ; leave #! lines first
    (if (looking-at "#!")
	(forward-line 1))
    ; file name and copyright
    (insert
     comment-start " File: " (file-name-nondirectory buffer-file-name) " " comment-end "\n"
     "\n"
     comment-start " Copyright (C) " (format-time-string "%Y" (current-time))
     " " user-full-name " <" user-mail-address "> " comment-end "\n"
     "\n")
    ; license abstract
    (while copying
      (insert comment-start " " (car copying) comment-end "\n")
      (setq copying (cdr copying)))
    ; stuff to print
    (insert
     comment-start "\"" (file-name-nondirectory buffer-file-name) " v0.1 (C) "
     (format-time-string "%Y" (current-time)) " " user-full-name "\"" comment-end "\n")
    ))
     


;;; Unicode & DOS
(defun dos2unix ()
  (interactive)
  (set-buffer-file-coding-system 'undecided-unix)
  (goto-char (point-min))
  (while (search-forward "\r" nil t) (replace-match "")))

(defun un-utfy (start end)
  "Remove silly characters from region"
  (interactive "r")
  (do-replace "ﬃ" "ffi" start end)
  (do-replace "ﬁ" "fi" start end)
  (do-replace "ﬀ" "ff" start end)
  (do-replace "ﬂ" "fl" start end)
  (do-replace "–" "-" start end)
  (do-replace "’" "'" start end)
  (do-replace "‘" "`" start end)
  (do-replace "Δ" "$\Delta$" start end)
  (do-replace "◦" "$^o$" start end)
  (do-replace "−" "-" start end)
  (do-replace "—" "-" start end)
  (do-replace "α" "$\alpha$" start end)
  (do-replace "€" "EUR" start end)
)



;;; Utilities
(defun up-slightly () (interactive) (scroll-up 5))
(defun down-slightly () (interactive) (scroll-down 5))
(defun left-slightly () (interactive) (scroll-left 5))
(defun right-slightly () (interactive) (scroll-right 5))
(defun up-slightly-other-window () (interactive) (scroll-other-window 5))
(defun down-slightly-other-window () (interactive) (scroll-other-window-down 5))

(defun p-prev-window () (interactive) (other-window -1 1))
(defun p-next-window () (interactive) (other-window 1 1))

(defun switch-to-next-buffer ()
  (interactive)
  (bury-buffer nil))

(defun run-through-buffers ()
  (interactive)
  (switch-to-buffer (car (nreverse (buffer-list)))))



;;; Parentheses
(defun match-next-paren (arg)
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
        ((looking-at "\\s\{") (forward-list 1) (backward-char 1))
       (t (forward-word (or arg 1)))))

(defun match-prev-paren (arg)
  (interactive "p")
  (cond ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
        ((looking-at "\\s\}") (forward-char 1) (backward-list 1))
       (t (backward-word (or arg 1)))))

;;;; CHECK FOR UNMATCHED PARENTHESES!!!
(defun find-misbalanced-parentheses ()
  "Finds the first misbalanced parenthesis in the buffer"
  (interactive)
  (let ((beg (point)) (end (point)))
    (while (< end (buffer-size))
      (condition-case data
	  (save-excursion
	    (forward-list 1)			; next closing paren
	    (setq end (point))
	    (backward-list 1)
	    (setq beg (point)))
	(scan-error
	 (goto-char (nth 2 data))
	 (error "ERROR!!!")))
      ;(message "found %d->%d" beg end)
      ;(message (buffer-substring beg end))
      (goto-char end)
      (when (/= (char-syntax (char-after beg)) ?\$)
	(if (not (eq (char-before end)
		     (matching-paren (char-after beg))))
	    (error "ERROR!!! found at %d-%d" beg end))))))



;;; Other
(defun what-face (pos)
  "Find the face of the character at point."
  (interactive "d")
  (let ((face (or (get-char-property (point) 'read-face-name)
                  (get-char-property (point) 'face))))
    (if face (message "Face: %s" face) (message "No face at %d" pos))))



(provide 'p-interactive)
;;; p-interactive.el ends here
