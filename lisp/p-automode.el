;;; p-automode.el -- Automode file types

;;; Commentary:
;;;   Automode binding for file extensions.

;;; Code:

(require 'python)
(require 'yaml-mode)
(require 'js)
(require 'erc-log)
(require 'erc-view-log)

(setq
 auto-mode-alist  (append
 		   '(("\\.lua\\'" . lua-mode)) ; LUA
 		   '(("\\.pyx\\'" . python-mode)) ; Cython
 		   '(("\\.cu\\'"  . c-mode))	  ; CUDA
		   '(("\\.m\\'"   . matlab-mode))
		   '(("\\.gp\\'" . gnuplot-mode))
		   '(("\\.xul\\'" . sgml-mode))
		   '(("\\.rdf\\'" . sgml-mode))
		   '(("\\.inc\\'" . html-mode))
		   '(("\\.namd\\'" . tcl-mode))
		   '(("\\(/\\|\\`\\)bash-fc-[0-9]+\\'" . sh-mode))
                   '(("NOTES_ogame\\'" . ogame-mode))
		   '(("DIARY" . org-mode))
		   '((".*ORG.*" . org-mode))
		   '((".*\\.pdb" . org-mode))
		   '((".*\\.dat" . org-mode))
		   '((".*\\.ptex" . latex-mode))
		   '(("\\.\\(fasta\\|fa\\|exp\\|ace\\|gb\\)\\'" . dna-mode))
		   '((".*\\.md" . markdown-mode))
		   auto-mode-alist))



(provide 'p-automode)
;;; p-automode.el ends here

