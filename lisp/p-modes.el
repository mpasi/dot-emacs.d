;; p-modes.el --- Modes configuration

;;; Commentary:
;;;   Modes loading and configuration.

;;; Code:

;; (require 'autoinsert)
;; (require 'secrets)
(require 'ox-reveal)
(require 'whitespace)

;; (require 'compile)
;; (setq compilation-scroll-output 'first-error)

;; (require 'ag)
;; (setq ag-highlight-search t
;;       ag-group-matches t
;;       ag-context-lines 3
;;       ag-search-stats t)

;; (require 'emmet-mode)
;; (add-hook 'sgml-mode-hook 'emmet-mode)

(require 'sgml-mode)
(setq sgml-debug nil)
;; reindent after tag is removed
(advice-add 'sgml-delete-tag :after '(lambda (arg) (indent-region (point-min) (point-max))))

;; (require 'tramp-cache)
;; (setq tramp-persistency-file-name (file-path-in-cache-dir "tramp"))

;; (require 'nsm)
;; (setq nsm-settings-file (file-path-in-cache-dir "network-security.data"))

;; (require 'abbrev)
;; (setq abbrev-file-name (file-path-in-cache-dir "abbrev_defs"))

;; (require 'pcache)
;; (setq pcache-directory (file-path-in-cache-dir "pcache"))

;; (require 'projectile)
;; (projectile-global-mode)
;; (setq projectile-known-projects-file (file-path-in-cache-dir "projectile-bookmarks.eld"))


;;;; Various hooks
(add-hook 'jde-mode-hook  (lambda ()  (c-toggle-auto-state -1)(c-set-style "pjava")))
(add-hook 'c-mode-hook  (lambda ()  (c-toggle-auto-state -1)))
(add-hook 'html-mode 'my-html-mode)
(add-hook 'signal-USR1-hook 'save-buffers-kill-emacs)
(add-hook 'header-mode 'iso-accents-mode)
(add-hook 'ange-ftp-process-startup-hook
	  (function
	   (lambda ()
	     (ange-ftp-send-cmd 
	      host user (list 'passive) "Use passive mode"))))
(add-hook 'octave-mode-hook 'my-octave-mode)
(add-hook 'matlab-mode-hook 'my-matlab-mode)
(add-hook 'fortran-mode-hook 'my-fortran-mode)
(add-hook 'gnuplot-mode-hook 'my-gnuplot-mode)
(add-hook 'js-mode-hook 'my-js-mode)

;;;; LUA mode
(autoload 'lua-mode "lua-mode" "Lua editing mode." t)

;;;; Markdown mode
(autoload 'markdown-mode "markdown-mode"
  "Major mode for editing Markdown files" t)

;;;; DNA mode
(autoload 'dna-mode "dna-mode" "Major mode for dna" t)


;;;; HTML
(defun my-html-mode ()
  "A comfortable html mode definition"
  (interactive)
  (iso-accents-mode t)
  (auto-fill-mode t))

;;;; POST
(defun my-post-mode ()
  "A comfortable post mode definition for mutt"
  (post-mode)
  (auto-fill-mode)
  (iso-accents-mode)
  (setq make-backup-files nil))

;;;; Octave
(defun my-octave-mode()
  "A few changes to octave bindings"
  (interactive)
  (local-set-key "\M-;" 'comment-dwim)
  )

;;;; Matlab
(defun my-matlab-mode()
  "A few changes to matlab bindings"
  (interactive)
  (local-set-key [(control meta \;)] 'comment-dwim)
  (local-set-key "\M-s" 'replace-string)
  (local-set-key "\C-\M-f" 'fill-region)
  )

;;;; Fortran
(defun my-fortran-mode()
  "A few changes to fortran bindings"
  (interactive)
  (setq
   comment-start "c"
   comment-padding " "
   fortran-do-indent 1
   fortran-if-indent 1
   fortran-structure-indent 1
   fortran-continuation-indent 1
   fortran-comment-region "c"
   fortran-blink-matching-if 't
   fortran-blink-matching-do 't)
  (local-set-key [(control meta \;)] 'comment-dwim)
  )

;;;; Gnuplot
(autoload 'gnuplot-mode "gnuplot" "Gnuplot Mode v0.6.0" t)
(defun my-gnuplot-mode()
  "A few changes to gnuplot bindings"
  (interactive)
  (local-set-key "\C-c\C-u" 'my-decrement-number-decimal)
  )
(setq gnuplot-program "/opt/local/bin/gnuplot")

;;;; JS
(defun my-js-mode()
  "Less is more with Multi-Web ..."
  (interactive)
  (setq indent-tabs-mode nil))


;;;; Flyspell
(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checking" t)
(autoload 'flyspell-delay-command "flyspell" "Delay on command." t)
(autoload 'tex-mode-flyspell-verify "flyspell" "" t) 
(setq flyspell-sort-corrections nil)
(autoload 'flyspell-babel-setup "flyspell-babel")
(setq ispell-list-command "--list")
(setq flyspell-is-on nil)

(defun my-fly-spell ()
  "An on/off flyspell-mode wrapper"
  (interactive)
  (cond (flyspell-is-on                                  ; if flyspell-mode is on
	 (flyspell-mode nil)                             ; toggle flyspell-mode
	 (setq flyspell-is-on nil))                      ; turn flag off
	((not flyspell-is-on)
	 (call-interactively 'ispell-change-dictionary)  ; else ask for dictionary to use,
	 (flyspell-mode t)
	 (local-set-key "\C-x," 'flyspell-goto-next-error)
	 (local-set-key "\C-x!" 'my-ispell-add-word)
	 (setq flyspell-is-on t)                         ; turn on and check buffer
	 (flyspell-buffer))
	)
  )

(defun my-ispell-add-word ()
  "Interacively add next word to personal dictionary"
  (interactive)
  (setq wab (ispell-get-word t))
  (flyspell-unhighlight-at (car (cdr wab)))
  (ispell-send-string (concat "*" (car wab) "\n"))
  (ispell-pdict-save t t))


;;;; MDP mode (GROMACS)
;(require 'bk-gromacs-mdp-mode)

;;;; Multi-web-mode
(require 'multi-web-mode)
(setq mweb-default-major-mode 'html-mode)
(setq mweb-tags '((php-mode "<\\?php\\|<\\? \\|<\\?\\|<\\?=" "\\?>")
                   (js-mode "<script +\\(type=\"text/javascript\"\\|language=\"javascript\"\\)[^>]*>" "</script>")
                   (css-mode "<style +type=\"text/css\"[^>]*>" "</style>")))
(setq mweb-filename-extensions '("php" "htm" "html" "ctp" "phtml" "php4" "php5"))
(multi-web-global-mode 1)

;(require 'beamer)
;(require 'abc-mode)
;(require 'emacs_jde)
;(require 'psgml-html)
;(require 'flyspell)
;(require 'mic-paren)
;(require 'post)

(provide 'p-modes)
;;; p-modes.el ends here
