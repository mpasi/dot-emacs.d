;;; p-calc.el --- Calculator stuff

;;; Commentary:
;;;   Utilities to do calculations in Emacs.

;;; Code:

;;; CALCULATOR key bindings
;; set init, set incr and get values from `seq init ... incr`
(define-prefix-command 'pcalc-functions-map)
(global-set-key "\C-cm" 'pcalc-functions-map)
(define-key pcalc-functions-map "i" 'pcalc-set-init)
(define-key pcalc-functions-map "I" 'pcalc-set-incr)
(define-key pcalc-functions-map "n" 'pcalc-get)
(global-set-key "\C-cu" 'my-increment-number-decimal)
(global-set-key "\C-cd" 'my-decrement-number-decimal)
(global-set-key "\C-ce" 'calc-embedded)
(global-set-key "\C-cq" 'p-quick-calc)
(global-set-key "\C-c\C-q" 'p-quick-calc-add)


(defun p-truncate (a n)
  (if (or (null n) (< n 0)) a
    (/ (float (round (* a (expt 10 n)))) (expt 10 n))))

(defun p-quick-calc (start end &optional prefix)
  "evaluate selected expression and make result available in kill ring; optional prefix is precision of output."
  (interactive "r\nP")
  (save-excursion
    (p-quick-calc-add start end prefix))
  (delete-region start end)
  (insert (car kill-ring-yank-pointer)))

(defun p-quick-calc-add (start end &optional prefix)
  "Evaluate selected expression and make result available in kill ring; optional prefix is precision of output."
  (interactive "r\np")
  (copy-region-as-kill start end)
  (let ((fmt "f")
	(result (calc-eval (car kill-ring-yank-pointer))))
    (if (null prefix) ()
	(setq result (format (format "%%.%df" prefix) (car (read-from-string result)))))
    (kill-new result)))

;; set variables
(setq-default
 pcalc-init 0
 pcalc-incr 1
 pcalc-n 0
 )
;; set init
(defun pcalc-set-init (arg)
  "Set initial value"
  (interactive "p")
  (setq pcalc-init arg)
  (setq pcalc-n arg)
  (message (format "set init to %d" arg)))
;; set incr
(defun pcalc-set-incr (arg)
  "Set increment value"
  (interactive "p")
  (setq pcalc-incr arg)
  (message (format "set incr to %d" arg)))
;; get next value
(defun pcalc-get (arg)
  "Get next value"
  (interactive "p")
  (insert (format "%d" pcalc-n))
  (setq pcalc-n (+ pcalc-n pcalc-incr)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq comment-macro
      [?\C-  ?\C-e left ?\C-\M-r ?\[ ?^ ?  ?\C-i ?\] right ?\M-\; tab ?\C-a])
(setq uncomment-macro
      [?\C-e backspace ?\C-a ?\C-  ?\C-e ?\M-\; ?\C-a ?\C-e ?\\ ?\C-a tab ?\C-x ?\(])
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; measures: np package


(defun my-increment-number-decimal (&optional arg)
  "Increment the number forward from point by 'arg'."
  (interactive "p*")
  (save-excursion
    (save-match-data
      (let (inc-by field-width answer)
        (setq inc-by (if arg arg 1))
        (skip-chars-backward "0123456789")
        (when (re-search-forward "[0-9]+" nil t)
          (setq field-width (- (match-end 0) (match-beginning 0)))
          (setq answer (+ (string-to-number (match-string 0) 10) inc-by))
          (when (< answer 0)
            (setq answer (+ (expt 10 field-width) answer)))
          (replace-match (format (concat "%0" (int-to-string field-width) "d")
                                 answer)))))))
(defun my-decrement-number-decimal (&optional arg)
  "Decrement the number forward from point by 'arg'."
  (interactive "p*")
  (my-increment-number-decimal (- 0 arg)))



(provide 'p-calc)
;;; p-calc.el ends here
