;;; p-bindings.el -- Custom key bindings

;;; Commentary:
;;;   Additional key bindings.

;;; Code:

;; (global-set-key (kbd "C-`") (lambda () (interactive) (dired user-emacs-directory)))
;; print buffer name
;; (global-set-key (kbd "M-p") (lambda () (interactive) (message buffer-file-name)))

;; (global-set-key (kbd "C-c M-r") 'revert-buffer)

;; (global-set-key (kbd "C-x M-k") 'kill-matching-buffers)

(global-set-key (kbd "C-M-`") 'whitespace-cleanup)
;; (global-set-key (kbd "C-c s") 'string-insert-rectangle)
;; (global-set-key (kbd "C-c C-!") 'ack/shell-command-on-region-replace)
;; indent whole buffer
(global-set-key (kbd "C-M-|") (lambda () (interactive)
                                (save-excursion
                                  (indent-region (point-min) (point-max)))))

;; (global-set-key (kbd "C-c M-e") 'eshell)
;; (global-set-key (kbd "C-c M-t") (lambda () (interactive) (ansi-term "/bin/bash")))

;; (global-set-key (kbd "C-c n") 'linum-mode)

(global-set-key [remap dabbrev-expand] 'hippie-expand)

;; (global-set-key (kbd "C-c C-S-a") 'ag-project-regexp)
;; (global-set-key (kbd "C-c C-S-f") 'ag-project-files)

(global-set-key (kbd "M-x") 'smex)

;; (global-set-key (kbd "C-+") 'er/expand-region)

;; (global-set-key (kbd "C-c C->") 'mc/edit-lines)
;; (global-set-key (kbd "C->") 'mc/mark-next-like-this)
;; (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
;; (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this-dwim)

;; (with-eval-after-load 'ag-mode
;;   (define-key ag-mode-map (kbd "<C-return>")
;;     'ack/ag-compile-goto-error-and-kill-search-buffer))

;; (with-eval-after-load 'go-mode
;;   (define-key go-mode-map (kbd "M-g f") 'gofmt))

;; (with-eval-after-load 'dired
;;   (define-key dired-mode-map (kbd "C-c C-q") 'wdired-change-to-wdired-mode))

;; (with-eval-after-load 'dired-subtree
;;   (define-key dired-mode-map (kbd "C-<right>") 'dired-subtree-insert)
;;   (define-key dired-mode-map (kbd "C-<left>") 'dired-subtree-remove))

;; (with-eval-after-load 'yaml-mode
;;   (define-key yaml-mode-map (kbd "C-m") 'newline-and-indent))

;; (with-eval-after-load 'python
;;   (define-key python-mode-map (kbd "C-c x") 'jedi-direx:pop-to-buffer)
;;   (define-key python-mode-map (kbd "C-c d") (lambda () (interactive)
;;                                               (ack/insert-text-and-reindent "import pdb; pdb.set_trace()"))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key [f2] 'eval-buffer)
(global-set-key [f3] 'my-fly-spell)
;(global-set-key [f4] 'java-run)
(global-set-key [C-f4] 'delete-window)
(global-set-key [f5] 'font-lock-fontify-buffer)
(global-set-key [S-f5] 'hs-show-block)
(global-set-key [f6] 'hs-hide-block)
;(global-set-key [f7] 'java-compile)
(global-set-key [f9] 'electric-buffer-list)
(global-set-key [f10] 'licenseify)

(global-set-key "\C-b" 'switch-to-next-buffer)
(global-set-key "\C-\M-b" 'run-through-buffers)
(global-set-key "\C-x\M-d" 'insert-date-short)
(global-set-key "\C-x\C-\M-d" 'insert-date)
(global-set-key "\C-xt" 'insert-time)
(global-set-key "\M-s" 'replace-string)
(global-set-key "\M-r" 'replace-regexp)
(global-set-key "\M-h" 'highlight-phrase)
(global-set-key "\C-\M-h" 'highlight-regexp)
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key "\C-x\C-z" 'undo)
;(global-set-key "\C-x\C-j" 'java-mode)
;(global-set-key "\C-x\C-m" 'mmm-parse-buffer)
(global-set-key "\C-f" 'my-justify-region)
(global-set-key "\C-\M-f" 'fill-region)
(global-set-key "\C-xf" 'my-set-fill-column)
(global-set-key (kbd "\C-c TAB") 'self-insert-command)
;(global-set-key "\C-cc" 'find-misbalanced-parentheses)
(global-set-key "\C-v" 'yank)
(global-set-key "\M-v" 'yank-pop)

;; SCROLLING and others
(global-set-key [C-next] 'scroll-other-window)
(global-set-key [C-prior] 'scroll-other-window-down)
(global-set-key [C-tab] 'p-next-window)
(global-set-key [C-S-tab] 'p-prev-window)
;(global-set-key [S-tab] 'p-prev-window)
(global-set-key [C-M-right] 'match-next-paren)
(global-set-key [C-M-left] 'match-prev-paren)
(global-set-key [C-M-return] 'hippie-expand)
(global-set-key [M-up] 'down-slightly)
(global-set-key [M-down] 'up-slightly)
(global-set-key [M-C-up] 'down-slightly-other-window)
(global-set-key [M-C-down] 'up-slightly-other-window)
(global-set-key [mouse-4] 'down-slightly)
(global-set-key [mouse-5] 'up-slightly)
(global-set-key [S-mouse-4] 'down-slightly-other-window)
(global-set-key [S-mouse-5] 'up-slightly-other-window)
(global-set-key [delete] 'delete-char)



(provide 'p-bindings)
;;; p-bindings.el ends here
