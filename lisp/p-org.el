;;; p-org.el --- ORG-mode stuff

;;; Commentary:
;;;   Setup and utilities for ORG-mode.

;;; Code:

(require 'org)
(require 'org-install)
;; (require 'remember-autoloads)
;; CONTRIB
(add-to-list 'org-modules 'ox-freemind)
(defun libxml-parse-xml-region (start end) ;required by ox-freemind
  "Adapter function: parse XML from START to END."
  (car (xml-parse-region start end)))
;; GENERAL
(setq org-startup-truncated t)
(eval-after-load 'org
  '(define-key org-mode-map [(control tab)] nil))
(setq org-treat-S-cursor-todo-selection-as-state-change nil)
(setq org-use-property-inheritance t)
(setq org-list-allow-alphabetical t)
;; ;; remember setup
;; (setq org-remember-templates
;;       '(("Tasks" ?t "* TODO %?\n  %i\n  %a" "~/ORG/remember")
;;         ("Appointments" ?a "* Appointment: %?\n%^T\n%i\n  %a" "~/ORG/remember")))
;; (setq remember-annotation-functions '(org-remember-annotation))
;; (setq remember-handler-functions '(org-remember-handler))
;; (eval-after-load 'remember
;;   '(add-hook 'remember-mode-hook 'org-remember-apply-template))
;; (global-set-key (kbd "C-c r") 'remember)
;;;;;;;;;;;;;; --------------
;;;;;;;;;;;;;; STATES AND UI
;;;; hierarchical TODO/DONE
(defun org-summary-todo (n-done n-not-done)
  "Switch entry to DONE when all subentries are done, to TODO otherwise.
Receives whether or not the element is N-DONE or N-NOT-DONE."
  (let (org-log-done org-log-states)
    (org-todo (if (= n-not-done 0) "DONE" "TODO"))))
(add-hook 'org-after-todo-statistics-hook 'org-summary-todo)
;;;; define states and colors
(setq org-todo-keywords (quote ((sequence "TODO(t)" "MAYBE(m)" "REDO(r)" "WAITING(w)" "ASSIGNED(a)" "DEAD(x)" "STARTED(s!)" "|" "DONE(d!/!)"))))
(setq org-todo-keyword-faces
      (quote
       (("TODO" :foreground "red" :weight bold)
        ("REDO" :foreground "tomato" :weight bold)
        ("WAITING" :foreground "coral" :weight bold)
        ("ASSIGNED" :foreground "magenta" :weight bold)
        ("DEAD" :foreground "gray20" :weight bold)
        ("STARTED" :foreground "blue" :weight bold)
        ("DONE" :foreground "forest green" :weight bold))))
(setq org-priority-faces
      (quote
       ((65 . "red1")
        (66 . "red3")
        (67 . "red4"))))
;; list-faces-display to show all faces
(set-face-attribute 'bold nil
                    :foreground "white")
(set-face-attribute 'org-level-1 nil
                    :foreground "LightSkyBlue")
(set-face-attribute 'org-level-2 nil
                    :foreground "Goldenrod")	     ; LightGoldenrod
(set-face-attribute 'org-level-3 nil
                    :foreground "Cyan3") ; Cyan1
(set-face-attribute 'org-level-4 nil
                    :foreground "DarkOrange2") ; chocolate1
(set-face-attribute 'org-level-5 nil
                    :foreground "PaleGreen") ; PaleGreen
(set-face-attribute 'org-level-6 nil
                    :foreground "Plum") ; Aquamarine
(set-face-attribute 'org-level-7 nil
                    :foreground "LightSteelBlue") ; LightSteelBlue
(set-face-attribute 'org-level-8 nil
                    :foreground "LightSalmon") ; LightSalmon
(set-face-attribute 'org-block-begin-line nil
                    :foreground "coral1") ; chocolate4
(set-face-attribute 'org-block-end-line nil
                    :foreground "coral1") ; chocolate4
(set-face-attribute 'org-block nil
                    :foreground "chocolate1") ; chocolate1
(set-face-attribute 'org-meta-line nil
                    :foreground "chocolate1") ; chocolate1
(set-face-attribute 'org-quote nil
                    :foreground "chocolate1") ; chocolate1
(set-face-attribute 'font-lock-comment-face nil
                    :foreground "chocolate1") ; chocolate1
(set-face-attribute 'org-special-keyword nil
                    :foreground "SteelBlue4") ; Cyan1
(set-face-attribute 'org-list-dt nil
                    :foreground "gray70") ; 
(set-face-attribute 'org-verbatim nil
                    :foreground "white") ; 
;;;;;;;;;;;;;; ------------
;;;;;;;;;;;;;; AGENDA SETUP
(global-set-key (kbd "C-c a") 'org-agenda)
(setq org-agenda-files (quote ("~/ORG/p" "~/ORG/sci")))
;; custom agenda views
;; (setq org-agenda-custom-commands
;;       (quote (
;;               ("s" "Started Tasks" todo "STARTED" ((org-agenda-todo-ignore-scheduled nil)
;;                                                    (org-agenda-todo-ignore-deadlines nil)
;;                                                    (org-agenda-todo-ignore-with-date nil)))
;;               ("t" "Tasks to do" todo "TODO")
;;               ("w" "Tasks waiting on something" todo "WAITING/!" ((org-use-tag-inheritance nil)))
;;               ;;              ("r" "Refile New Notes and Tasks" tags "LEVEL=1+REFILE" ((org-agenda-todo-ignore-with-date nil)))
;;               ;;              ("N" "Notes" tags "NOTE" nil)
;;               ;;              ("n" "Next" tags "NEXT-WAITING-CANCELLED/!" nil)
;;               ;;              ("p" "Projects" tags-todo "LEVEL=2-NEXT-WAITING-CANCELLED/!-DONE" nil)
;;               ;;              ("A" "Tasks to be Archived" tags "LEVEL=2/DONE|CANCELLED" nil)
;; )))
;; (setq org-agenda-include-diary t)
;; (setq org-agenda-include-all-todo t)
;; Agenda log mode items to display (clock time only by default)
;; (setq org-agenda-log-mode-items (quote (clock)))
;;;;;;;;;;;;;; --------------
;;;;;;;;;;;;;; CLOCKING SETUP
;; Resume clocking tasks when emacs is restarted
;; (org-clock-persistence-insinuate)
;; Yes it's long... but more is better ;)
;; (setq org-clock-history-length 35)
;; Resume clocking task on clock-in if the clock is open
;; (setq org-clock-in-resume t)
;; Change task state to STARTED when clocking in
;; (setq org-clock-in-switch-to-state "STARTED")
;; Separate drawers for clocking and logs
;; (setq org-drawers (quote ("PROPERTIES" "LOGBOOK" "CLOCK")))
;; Save clock data in the CLOCK drawer and state changes and notes in the LOGBOOK drawer
;; (setq org-clock-into-drawer "CLOCK")
;; (setq org-clock-into-drawer 'nil)
;; Sometimes I change tasks I'm clocking quickly - this removes clocked tasks with 0:00 duration
;; (setq org-clock-out-remove-zero-time-clocks t)
;; Don't clock out when moving task to a done state
;; (setq org-clock-out-when-done nil)
;; Save the running clock and all clock history when exiting Emacs, load it on startup
;; (setq org-clock-persist nil)
;; (setq org-clock-persist (quote history))
;; automatic clocking: clock in when task is marked started
;; (eval-after-load 'org
;;   '(progn
;;      (defun wicked/org-clock-in-if-starting ()
;;        "Clock in when the task is marked STARTED."
;;        (when (and (string= state "STARTED")
;; 		  (not (string= last-state state)))
;; 	 (org-clock-in)))
;;      (add-hook 'org-after-todo-state-change-hook
;; 	       'wicked/org-clock-in-if-starting)
;;      (defadvice org-clock-in (after wicked activate)
;;       "Set this task's status to 'STARTED'."
;;       (org-todo "STARTED"))
;;      ))
;;     (defun wicked/org-clock-out-if-waiting ()
;;       "Clock out when the task is marked WAITING."
;;       (when (and (string= state "WAITING")
;;                  (equal (marker-buffer org-clock-marker) (current-buffer))
;;                  (< (point) org-clock-marker)
;; 	         (> (save-excursion (outline-next-heading) (point))
;; 		    org-clock-marker)
;; 		 (not (string= last-state state)))
;; 	(org-clock-out)))
;;     (add-hook 'org-after-todo-state-change-hook
;; 	      'wicked/org-clock-out-if-waiting)))
;;;;;;;;;;;;;; ---------
;;;;;;;;;;;;;; EXPORTING
;;;;;; Latex
(setq org-export-skip-text-before-1st-heading t
      org-latex-tables-booktabs t
      org-latex-listings 'minted
      org-latex-packages-alist '(("" "minted"))
      org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
;; (setq org-export-latex-hyperref-format "\\ref{%s}")
;;;; Customise headlines: momentarily empty
;; (defun my-org-latex-format-headline-function (todo todo-type priority text tags)
;;   (org-latex-format-headline-default-function todo todo-type priority text tags))
;; (setq org-latex-format-headline-function 'my-org-latex-format-headline-function)
;;;;;;;;;;;;;; ---------------
;;;;;;;;;;;;;; ORG / BABEL setup
(setq org-src-fontify-natively t)
(setq org-src-preserve-indentation t)
;; (setq org-babel-octave-shell-command "/opt/local/bin/octave -q")
;; (setq org-babel-python-command "/opt/local/bin/python2.7")
(setq org-confirm-babel-evaluate nil)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((octave . t)
   (shell . t)
   (python . t)
   (dot . t)
   (gnuplot .t)
   (R .t)
   ))
;;;;;;;;;;;;;; ORG-MOBILE?
;; ;; Set to the location of your Org files on your local system
;; (setq org-directory "~/ORG")
;; ;; Set to the name of the file where new notes will be stored
;; (setq org-mobile-inbox-for-pull "~/ORG/flagged.org")
;; ;; Set to <your Dropbox root directory>/MobileOrg.
;; (setq org-mobile-directory "~/Dropbox/Apps/MobileOrg")
;;;;;;;;;;;;;; ORG: EASY TEMPLATES
(require 'org-tempo)
;; (setq org-structure-template-alist
;;       (append (list
;;                '("m" "#+BEGIN_MATH\n?\n#+END_MATH" "<literal style=\"math\">\n?\n</literal>")
;;                ) org-structure-template-alist))


(setq-default
 org-hide-leading-stars t
 org-odd-levels-only t)

(add-hook 'org-mode-hook 'my-org-mode)
(defun my-org-mode ()
  "Local definitions for ORG."
  (interactive)
  (fset 'orgtbl-send-table 'my-orgtbl-send-table)
  (message "My Org Mode")
  (local-set-key "\C-c\C-q" 'p-quick-calc-add)
                                        ;(global-set-key (kbd "C-<return>") (lambda () (interactive) (org-insert-heading 1)))
  (local-set-key (kbd "C-<return>") 'org-insert-heading-after-current))


(defun orgify (start end)
  "Insert useful header in org-only files.
Using information in the region START - END."
  (interactive "r")
  ;; XXX check it hasn't already been orgified
  (kill-region start end)
  (save-excursion
    (goto-char (point-min))
    (insert
     "#+DUMMY:\t-*- mode: org -*-" (format-time-string "%Y%m%d" (current-time)) "\n"
     "#+TITLE:\t" (car kill-ring-yank-pointer) "\n"
     "#+AUTHOR:\t" user-full-name "\n"
     "#+EMAIL:\t" user-mail-address "\n"
     "#+OPTIONS:\tH:3 num:t toc:t \\n:nil @:t ::t |:t ^:nil -:t f:t *:t TeX:t LaTeX:nil skip:t d:nil tags:not-in-toc\n"
     "#+DRAWERS:\tCODE NOTE IDEA\n\n"))
  (org-mode))

(defun org-tabify (start end)
  "Make the region (START - END) a org-table, converting s/[ \t]+/|/g."
  (interactive "r")
  (do-replace "^ *" "|" start end)
  (do-replace "[ 	]+" "|" start end)
  )


;;;;;;;;;;;;;
(defun my-orgtbl-send-table (&optional maybe)
  "Send a transformed version of this table to the receiver position.
With argument MAYBE, fail quietly if no transformation is defined for
this table."
  (interactive)
  (catch 'exit
    (unless (org-at-table-p) (error "Not at a table"))
    ;; when non-interactive, we assume align has just happened.
    (when (called-interactively-p "any") (org-table-align))
    (let ((dests (orgtbl-gather-send-defs))
          (txt (buffer-substring-no-properties (org-table-begin)
                                               (org-table-end)))
          (ntbl 0))
      (unless dests (if maybe (throw 'exit nil)
                      (error "Don't know how to transform this table")))
      (dolist (dest dests)
        (let* ((name (plist-get dest :name))
               (transform (plist-get dest :transform))
               (params (plist-get dest :params))
               (skip (plist-get params :skip))
               (skipr (plist-get params :skipr)) ;p: added
               (where (plist-get params :where)) ;p: added
               (skipcols (plist-get params :skipcols))
               (no-escape (plist-get params :no-escape))
               beg
               (lines (org-table-clean-before-export
                       (nthcdr (or skip 0)
                               (org-split-string txt "[ \t]*\n[ \t]*"))))
               (i0 (if org-table-clean-did-remove-column 2 1))
               (lines (if no-escape lines
                        (mapcar (lambda(l) (replace-regexp-in-string
                                            "\\([&%#_^]\\)" "\\\\\\1{}" l)) lines)))
               (lines2 (org-remove-by-index lines skipr))
               (table (mapcar		;p: removed skipcols from here
                       (lambda (x)
                         (if (string-match org-table-hline-regexp x)
                             'hline
                           (org-split-string (org-trim x) "\\s-*|\\s-*")))
                       lines2))
               (table2 (if where	;p: added where clause -> table2
                           (org-remove-if
                            (lambda (e)
                              (if (not (listp e)) e
                                (not (equal (nth (1- (car where)) e) (cadr where))))) table)
                         table))
               (table (mapcar		;p: restored skipcols here -> table
                       (lambda (x)
                         (if (not (listp x)) x
                           (org-remove-by-index x skipcols i0))) table2))
               (fun (if (= i0 2) 'cdr 'identity))
               (org-table-last-alignment
                (org-remove-by-index (funcall fun org-table-last-alignment)
                                     skipcols i0))
               (org-table-last-column-widths
                (org-remove-by-index (funcall fun org-table-last-column-widths)
                                     skipcols i0))
               (txt (if (fboundp transform)
                        (funcall transform table params)
                      (user-error "No such transformation function %s" transform))))
          (orgtbl-send-replace-tbl name txt))
        (setq ntbl (1+ ntbl)))
      (message "Table converted and installed at %d receiver location%s"
               ntbl (if (> ntbl 1) "s" ""))
      (if (> ntbl 0)
          ntbl
        nil))))
;;;;;;;;;;;;;
(defun org-get-line-number-in-block ()
  (interactive)
  (message "line %d" (org-line-number-in-block)))
;;;;;;;;;;;;;
(defun org-line-number-in-block (&optional pos)
  "Return BLOCK line number at position POS.
If POS is nil, use current buffer location."
  (let ((opoint (or pos (point))) start)
    (save-excursion
      (org-next-block 1 t "#\\+BEGIN") ;
      (setq start (point))
      (goto-char opoint) 
      (forward-line 0)
      (count-lines start (point)))))
;;;;;;;;;;;;;
(defun org-transpose-table-at-point ()
  "Transpose orgmode table at point, eliminate hlines"
  (interactive)
  (let ((contents
         (apply #'mapcar* #'list
                ;; remove 'hline from list
                (remove-if-not 'listp
                               ;; signals error if not table
                               (org-table-to-lisp)))))
    (delete-region (org-table-begin) (org-table-end))
    (insert (mapconcat (lambda(x) (concat "| " (mapconcat 'identity x " | " ) "  |\n" ))
                       contents ""))
    (org-table-align)))


(provide 'p-org)
;;; p-org.el ends here
