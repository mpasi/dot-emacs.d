;;; p-private.el --- Private stuff

;;; Commentary:
;;;   Configurations that are not to be shared.

;;; Code:
(setq-default
 user-full-name "Marco Pasi"
 user-mail-address "mf.pasi@gmail.com"
)

(provide 'p-private)
;;; p-private.el ends here
