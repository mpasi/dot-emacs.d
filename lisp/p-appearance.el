;;; p-appearance.el --- Appearance settings

;;; Commentary:
;;;   Appearance and themes settings.

;;; Code:

(setq inhibit-startup-message t
      initial-scratch-message nil
      inhibit-startup-echo-area-message t)

;; (setq initial-frame-alist '((fullscreen . maximized)))
 (add-to-list 'default-frame-alist '(width . 200))
 (add-to-list 'default-frame-alist '(height . 50))

;; display Emacs version and buffer name in window title
(setq frame-title-format
      '("Emacs " emacs-version ": "
        (:eval (if (buffer-file-name) (abbreviate-file-name (buffer-file-name)) "%b"))))

(setq-default
 indicate-empty-lines t
 x-stretch-cursor t
 ;; higlight
 font-lock-maximum-decoration t
 ;; misc
 display-time-24hr-format t
 truncate-lines t
 truncate-partial-width-windows nil
 visible-bell nil
 confirm-kill-emacs nil
 compilation-window-height '8
)

(defun my-terminal-visible-bell ()
  "A friendlier visual bell effect."
  (invert-face 'mode-line)
  (run-with-timer 0.1 nil 'invert-face 'mode-line))

(setq ring-bell-function #'my-terminal-visible-bell)

(tool-bar-mode -1)
(menu-bar-mode -1)
(when (display-graphic-p)
  (scroll-bar-mode -1))
(winner-mode t)

(global-hl-line-mode t)
(column-number-mode t)
(which-function-mode t)
(show-paren-mode t)

;; p
(delete-selection-mode t)
(global-font-lock-mode t)
(line-number-mode t)

(copy-face 'highlight 'isearch)
(display-time)


;; themes configuration

;; (require 'color-theme)
;; (require 'color-theme-solarized)

(setq custom-theme-directory (concat user-emacs-directory "themes")
      custom-safe-themes t
      frame-background-mode 'dark)

(load-theme 'base16-default-dark)
;; (load-theme 'solarized t)
;; (load-theme 'ack-solarized t)
(load-theme 'ack-common t)
(load-theme 'p-common t)

(provide 'p-appearance)
;;; p-appearance.el ends here
