;;; p-backup.el --- Backup and autosave

;;; Commentary:
;;;   Backup and austosave configuration.

;;; Code:

;; (setq auto-save-file-name-transforms '((".*" "~/.emacs-saves/autosaves/\\1" t))
;;       auto-save-list-file-prefix "~/.emacs-saves/autosaves/saves-")

(setq backup-directory-alist '((".*" . "~/.backup/"))
      backup-by-copying t
      delete-old-versions t
      kept-new-versions 10
      kept-old-versions 2
      version-control t)

(persistent-scratch-setup-default)

(provide 'p-backup)
;;; p-backup.el ends here
