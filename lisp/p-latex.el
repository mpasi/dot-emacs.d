;;; p-latex.el --- LaTeX stuff

;;; Commentary:
;;;   Setup and utilities to edit LaTeX.

;;; Code:

(autoload 'LaTeX-preview-setup "preview")
(setq-default
 latex-math-mode-is-on nil ; personal var for latex-math-mode
 )

(add-hook 'latex-mode-hook 'flyspell-babel-setup)
(add-hook 'LaTeX-mode-hook 'my-tex-mode)

(defun latex-insert-tag (tag)
  "Insert a tag; if selection is active, wrap selection in tag."
  (interactive)
  (setq init (concat "\\" tag "{"))
  (setq end "}")
  (if mark-active
      (progn
	(setq pos (sort (list (point) (mark)) '<))
	(message "%s" pos)
	(goto-char (pop pos))
	(insert init)
	(goto-char (+ (pop pos) (length init)))
	(insert end))
    (progn
     (insert (concat init end))
     (backward-char 1))))


(defun my-tex-mode()
  "A comfortable tex-mode definition for editing tex files in italian"
  (interactive)
  (LaTeX-preview-setup)
  (auto-fill-mode t)
  (setq standard-display-european t)
  ;(iso-accents-mode t)
  (reftex-mode t)
  (setq ispell-parser 'tex)
  ;(setq-default ispell-program-name "aspell")
  (setq ispell-check-comments nil)
  (turn-on-orgtbl)
  (turn-on-orgstruct++)
  (local-set-key "\C-c~" 'my-latex-math-mode)
  (local-set-key "\C-ce" (lambda () (interactive) (latex-insert-tag "emph")))
  (local-set-key "\C-cs" (lambda () (interactive) (latex-insert-tag "textbf")))
  (local-set-key "\C-cb" (lambda () (interactive) (latex-insert-tag "textbf")))
  ;;(local-set-key "\C-cx" 'latex-insert-xxx)
  ;;(local-set-key (kbd "\C-c TAB") 'self-insert-command)
  (local-set-key "\C-ci" (lambda () (interactive) (insert "\\item<+->")(backward-char 3)))
  (local-set-key (kbd "\C-c <down>") (lambda () (interactive) (insert "\\down{}")))
  (local-set-key (kbd "\C-c <up>") (lambda () (interactive) (insert "\\up{}")))
  (local-set-key "\C-ct" 'latex-align-numbers-table)
  (define-key reftex-mode-map (kbd "\C-c\\") 'LaTeX-close-environment)
  ;; LinAlg Math Tex (EPFL 121023)
  (local-set-key "\C-cv" (lambda () (interactive) (latex-insert-tag "bp")))
  ;; END
  ;; fontify cheetah
  (font-lock-add-keywords
   nil
   '(
     ("\\(#\\(from\\|else\\|include\\|extends\\|set\\|def\\|import\\|for\\|if\\|end\\)+\\)\\>" 1 font-lock-warning-face)
     ("\\(#\\(from\\|for\\|end\\)\\).*\\<\\(for\\|import\\|def\\|if\\|in\\)\\>" 3 font-lock-warning-face)
     ("\\(##.*\\)\n" 1 font-lock-comment-face)
     ("\\(\\$\\(?:\\sw\\|}\\|{\\|\\s_\\)+\\)" 1 font-lock-variable-name-face))
   )
  ;; (modify-syntax-entry "\\(<\\)!--" (1 "< b"))
  ;; ("--[ 	\n]*\\(>\\)"
  ;;  (1 "> b"))
  ;;
  (message "My-TeX-Mode: done")
  )

(defun my-latex-math-mode()
  "Wrap latex-math-mode to remove iso-accents-mode"
  (interactive)
  (cond (latex-math-mode-is-on
	 (iso-accents-mode t)
	 (latex-math-mode nil)
	 (setq latex-math-mode-is-on nil))
	((not latex-math-mode-is-on)
	 (iso-accents-mode nil)
	 (latex-math-mode t)
	 (setq latex-math-mode-is-on t))
	)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; LATEX OPENOFFICE STUFF ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (defun de-ooize-latex (start end)
;;   "removes shit from ooffice exported latex"
;;   (interactive "r")
;;   (save-excursion
;;     (do-replace "\\\\textcolor\\(\\[rgb\\]\\)?{[^}]+}{" "\\\\XXX{" start end)
;;     (do-replace "{\\\\selectlanguage{[^}]+}" "" start end)
;;     (do-replace "\\\\foreignlanguage{[^}]+}{" "" start end)))
;;  
;; (global-set-key "\C-cv" 'de-ooize-latex)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun latex-align-numbers-table (start end)
  "format a series of table lines according to a well formatted first line.
Table must be prepared with (consider implementing as first op):
  - replace-regexp ' *& *' ' & '
  - replace-regexp ' *\\\\' ' \\\\' [implemented]
"
  (interactive "r")
  (setq DEBUG '1)
  
  (save-excursion
    (goto-char start)
    (do-replace " *\\\\\\\\" " \\\\\\\\" start (+ end 4))
    (goto-char start)
    ;; NUMBER OF LINES
    (setq rows (count-lines start end))
    ;; NUMBER OF COLUMNS
    (back-to-indentation)
    (setq cols (cons (current-column) '()))
    (while (re-search-forward "&" (line-end-position) t)
      (push (current-column) cols))
    (if (re-search-forward "\\\\\\\\" (line-end-position) t)
	(push (current-column) cols)
      (push (move-to-column (point-max)) cols))
    (setq cols (reverse cols))
    (if (= DEBUG 1) (message "%s" cols))
    
    ;; EACH COLUMN
    (setq cinit (pop cols))
    (condition-case ()
	(while cols
	  ;; reset position
	  (goto-char start)
	  ;; consume cols
	  (setq cend (pop cols))
	  ;; first line:
	  ;;  understand positions
	  (move-to-column cend)
	  (setq pend (point))
	  (move-to-column cinit)
	  (setq pinit (point))
	  ;; look for dot
	  (setq pdot (re-search-forward "\\.\\{1\\}" pend t))
	  (setq cdot (current-column))
	  (if (= DEBUG 1) 
	      (if (= cdot cinit)
		  (message "%d(%d):%d(%d)" pinit cinit pend cend)
		(message "%d(%d):%d(%d):%d(%d)" pinit cinit pdot cdot pend cend)))
	  
	  ;; every other line
	  (condition-case data
	      (dotimes (line rows)
		(forward-line 1)
		;;  understand positions
		(move-to-column cend)
		(setq pend (point))
		(move-to-column cinit)
		(setq pinit (point))
		;; look for dot and end
		(re-search-forward "\\.\\{1\\}" pend t)
		(setq tmpde (current-column))
		(re-search-forward "\\(&\\|\\\\\\\\\\)" pend t)
		(setq tmpce (current-column))
		
		(if (= DEBUG 1)
		    (message "add%d: %d->%d %d->%d (%s)" line
			     tmpde cdot tmpce cend
			     (buffer-substring (line-beginning-position) (line-end-position))))
		(if (= tmpce 0)
		    (error))
		;; ADD SPACES
		(if (not (= tmpde cinit))
		  ;; initial spaces
		    (progn
		      (move-to-column cinit)
		      (setq initadd (max (- cdot tmpde) 0))
		      (dotimes (i initadd)
			(insert " ")))
		  (setq initadd 0))
		;; final spaces
		(move-to-column (+ tmpce (- initadd 1)))
		(setq endadd (max (- cend tmpce) 0))
		(dotimes (i (- endadd initadd))
		  (insert " ")))
	  (error
	   (message "line is wrong (%s) [%s]" 
		    (buffer-substring (line-beginning-position) (line-end-position))
		    (nth 1 data))))
	  ;(message "next: %s" cols)
	  (setq cinit cend)))))


;;;;;;;;;;;;;
(defun la-mats (table params)
  ""
  (let ((ncol (plist-get params :ncol))
	 (mcol (plist-get params :mcol))
	 (params2
	  (list
	   :tstart "\\begin{bmatrix}"
	   :tend (concat "\\end{bmatrix}" (plist-get params :msep) "\n")
	   :lstart "" :lend " \\\\" :llend "" :sep " & "
	   :efmt "%s\\,(%s)" :hline "\\hline"))
	 (stage 0)
	 ttable)
    (while (< stage ncol)
      ; XXX when last, s/thicksim/./
      (setq ttable (concat ttable (orgtbl-to-generic
				   (mapcar
				    (lambda (x)
				      (org-remove-by-index x
							   (nconc (number-sequence 1 stage)
								  (number-sequence (+ stage mcol 1) ncol))
							   1)) table)
				   (org-combine-plists params2 params))))
      (setq stage (+ stage mcol)))
    ttable))

(provide 'p-latex)
;;; p-latex.el ends here
