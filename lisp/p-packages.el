;;; p-packages.el --- Package configuration

;;; Commentary:
;;;   Package system and repositories configuration.

;;; Code:

(require 'package)

(setq package-user-dir "~/share/emacs/packages"
      package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("melpa" . "http://melpa.org/packages/")
                         ("melpa-stable" . "http://stable.melpa.org/packages/")
;                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")
                         ("elpy" . "http://jorgenschaefer.github.io/packages/")))
 (package-initialize)
;(package-refresh-contents)
;(package-install-selected-packages)

(provide 'p-packages)
;;; p-packages.el ends here
