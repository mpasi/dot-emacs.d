;;; p-common-theme-el --- Further customization

;;; Commentary:
;;;   Further customizations to existing themes

;;; Code:

(deftheme p-common "Further customizations.")

(custom-theme-set-faces
 'p-common
 '(mode-line ((t (:foreground "#00ffff" :background "#383838" :inverse-video t :box nil))))
 )

(provide-theme 'p-common)
;;; p-common-theme.el ends here
